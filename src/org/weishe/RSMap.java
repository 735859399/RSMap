package org.weishe;

import java.util.ArrayList;
import java.util.HashMap;

public class RSMap<E extends Comparable, T> {

	public static final int SORT_DESC = 0;// ����
	public static final int SORT_ASC = 1;// ����

	private Sort s;

	public enum Sort {
		DESC, ASC
	}

	public RSMap(Sort s) {
		this.s = s;
	}

	private ArrayList<E> key = new ArrayList<E>();
	private ArrayList<T> value = new ArrayList<T>();

	public void add(E k, T v) {
		switch (s) {
		case DESC:
			addDesc(k, v);
			break;

		case ASC:
			addAsc(k, v);
			break;
		}
	}

	/**
	 * ��������
	 */
	private void addDesc(E k, T v) {
		for (int i = 0; i < key.size(); i++) {
			if (k.compareTo(key.get(i)) > 0) {
				key.add(i, k);
				value.add(i, v);
				return;
			}
		}

		key.add(k);
		value.add(v);

	}

	/**
	 * ��������
	 */
	private void addAsc(E k, T v) {
		for (int i = 0; i < key.size(); i++) {
			if (k.compareTo(key.get(i)) < 0) {
				key.add(i, k);
				value.add(i, v);
				return;
			}
		}
		key.add(k);
		value.add(v);
	}

	public void clear() {
		key.clear();
		value.clear();
	}

	public ArrayList<HashMap<E, T>> getByKey(E k) {

		ArrayList<HashMap<E, T>> list = new ArrayList<HashMap<E, T>>();

		for (int i = 0; i < key.size(); i++) {
			if (k.compareTo(key.get(i)) == 0) {
				HashMap<E, T> m = new HashMap<E, T>();
				m.put(key.get(i), value.get(i));
				list.add(m);
			}

		}
		return list;
	}

	public HashMap<E, T> get(int index) {
		HashMap<E, T> m = new HashMap<E, T>();
		m.put(key.get(index), value.get(index));
		return m;
	}

	public int size() {
		return key.size();
	}
}
