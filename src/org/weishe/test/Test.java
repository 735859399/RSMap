package org.weishe.test;

import java.util.HashMap;

import org.weishe.RSMap;
import org.weishe.RSMap.Sort;

public abstract class Test {

	public static void main(String[] args) {
		RSMap<Integer, String> m = new RSMap<Integer, String>(Sort.ASC);
		m.add(2, "aaaaaaaaaaaaaa");
		m.add(1, "aaaaaaaaaaaaaa");

		m.add(1, "bbbbbbbbbbbbbbb");
		m.add(6, "aaaaaaaaaaaaaa");

		for (int i = 0; i < m.size(); i++) {
			HashMap<Integer, String> hm = m.get(i);
			System.out.println(hm);
		}
	}

}
